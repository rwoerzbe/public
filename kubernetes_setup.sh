#!/bin/bash -vx

APPNAME="k8s"

source <(curl -s https://gitlab.com/rwoerzbe/public/-/raw/main/generic_setup.sh)

if [[ -z "${COMMIT}" ]]; then
  COMMIT="45753fc989eca592eaf10d07986243cfc261c187";
fi

if [ "${STAGE}" -ge 1 ]
then
gcloud projects create ${APPNAME}-${CAMPUS_ID}-${PROJECT_ID_SUFFIX}
gcloud config set project ${APPNAME}-${CAMPUS_ID}-${PROJECT_ID_SUFFIX}
gcloud beta billing projects link ${DEVSHELL_PROJECT_ID} --billing-account=${BILLING}
gcloud services enable compute.googleapis.com
gcloud config set compute/region ${REGION}
gcloud config set compute/zone ${ZONE}
fi

if [ "${STAGE}" -ge 2 ]
then
sudo apt-get install kubectl -y
fi

if [ "${STAGE}" -ge 3 ]
then
gcloud services enable container.googleapis.com
gcloud container clusters create guestbook --num-nodes=2
fi

if [ "${STAGE}" -ge 4 ]
then
kubectl create -f https://raw.githubusercontent.com/GoogleCloudPlatform/kubernetes-engine-samples/${COMMIT}/guestbook/redis-leader-deployment.yaml
fi

if [ "${STAGE}" -ge 5 ]
then
kubectl create -f https://raw.githubusercontent.com/GoogleCloudPlatform/kubernetes-engine-samples/${COMMIT}/guestbook/redis-leader-service.yaml
fi

if [ "${STAGE}" -ge 6 ]
then
kubectl create -f https://raw.githubusercontent.com/GoogleCloudPlatform/kubernetes-engine-samples/${COMMIT}/guestbook/redis-follower-deployment.yaml
fi

if [ "${STAGE}" -ge 7 ]
then
kubectl create -f https://raw.githubusercontent.com/GoogleCloudPlatform/kubernetes-engine-samples/${COMMIT}/guestbook/redis-follower-service.yaml
fi

if [ "${STAGE}" -ge 8 ]
then
kubectl create -f https://raw.githubusercontent.com/GoogleCloudPlatform/kubernetes-engine-samples/${COMMIT}/guestbook/frontend-deployment.yaml
fi

if [ "${STAGE}" -ge 9 ]
then
curl -sL https://raw.githubusercontent.com/GoogleCloudPlatform/kubernetes-engine-samples/${COMMIT}/guestbook/frontend-service.yaml | sed "s/type: LoadBalancer//g" | kubectl create -f -
fi

# There is nothing to automate in stage 10

if [ "${STAGE}" -ge 11 ]
then
      kubectl apply -f https://raw.githubusercontent.com/GoogleCloudPlatform/kubernetes-engine-samples/${COMMIT}/guestbook/frontend-service.yaml
      while [ "$(kubectl get service -l=tier=frontend -o=jsonpath='{.items[0].status.loadBalancer.ingress[0].ip}')" = "" ]
      do
            sleep 5
      done
      echo EXTERNAL_IP is $(kubectl get service -l=tier=frontend -o=jsonpath='{.items[0].status.loadBalancer.ingress[0].ip}')
fi

# There is nothing to automate in stages 12 - 14

if [ "${STAGE}" -ge 15 ]
then
curl -sL https://raw.githubusercontent.com/GoogleCloudPlatform/kubernetes-engine-samples/${COMMIT}/guestbook/frontend-deployment.yaml | sed "s/replicas: 3/replicas: 2/g" | kubectl apply -f -
fi

if [ "${STAGE}" -ge 16 ]
then
git clone https://github.com/kubernetes/examples.git ${DEVSHELL_PROJECT_ID}-examples
git -C ${DEVSHELL_PROJECT_ID}-examples checkout 858d465
sed "s/btn-primary/btn-secondary/g" ~/${DEVSHELL_PROJECT_ID}-examples/guestbook/php-redis/index.html
docker image build --tag gb-frontend:v5 ~/${DEVSHELL_PROJECT_ID}-examples/guestbook/php-redis/
docker image tag gb-frontend:v5 gcr.io/${DEVSHELL_PROJECT_ID}/gb-frontend:v5
docker image push gcr.io/${DEVSHELL_PROJECT_ID}/gb-frontend:v5
kubectl set image deployment frontend php-redis=gcr.io/${DEVSHELL_PROJECT_ID}/gb-frontend:v5 --record=true
fi

if [ "${STAGE}" -ge 17 ]
then
kubectl rollout undo deployment frontend
fi