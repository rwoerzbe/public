#!/bin/bash

git init "$1"

# 1. Commit erzeugen
echo 'class HelloWorld {
  public static void main(String[] args) {
    System.out.println("+---------------------------+");
    System.out.println("|       Hallo World!        |");
    System.out.println("+---------------------------+");
  }
}' > "$1/HelloWorld.java"
git -C "$1" add HelloWorld.java
mkdir "$1/doc"
echo 'HelloWorld gibt "Hello World" auf dem Standard-Output-Stream aus.' > "$1/doc/README.txt"
git -C "$1" add doc/README.txt
git -C "$1" commit -m "Initiale Version von HelloWorld.java und doc/README.txt"

# 2. Commit auf req01
git -C "$1" checkout -b req01
sed -i '3i \ \ \ \ System.out.println("Herzlich Willkommen\\n");' "$1/HelloWorld.java"
echo "" >> "$1/doc/README.txt" #ToDo: sed mag keine \n am Anfang des Ersetzungstexts
sed -i '2a Change Log:\n\n* Kasten um Hello World' "$1/doc/README.txt"
git -C "$1" commit -am "Willkommensgruß eingefügt"

# 3. Commit auf master
git -C "$1" checkout master
sed -i 's/Hallo World!/Hello World!/g' "$1/HelloWorld.java"
echo "" >> "$1/doc/README.txt" #ToDo: sed mag keine \n am Anfang des Ersetzungstexts
sed -i '2a Bugfix:\n\n* Typo: "Hello World" statt "Hallo World"' "$1/doc/README.txt"
git -C "$1" commit -am "Typo beseitigt"

 

