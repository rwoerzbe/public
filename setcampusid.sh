#!/bin/bash

if [[ "$1" =~ ^[a-z][0-9a-z]{2,7}$ ]]; then
  echo "export CAMPUS_ID=${1}" >> ~/.bashrc 
  echo "export CAMPUS_ID=${1}" >> ~/.zshrc
  source ~/.bashrc
else
   echo "'$1' is not a valid campus ID. A campus ID consists of 3 to 8 lowercase letters where the last letter may also be a number,
i.e., something like 'mmuelle6'; don't mix it up with your matriculation number."
fi