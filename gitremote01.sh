#!/bin/bash

if [ -d "$1" ]; then
  echo 'directory $1 already exists'
  exit 1
fi

mkdir -p "$1"

git init --bare "$1/remoteRepo"

git clone "$1/remoteRepo" "$1/localRepo1"
git clone "$1/remoteRepo" "$1/localRepo2"

# 1. Commit erzeugen
echo 'class HelloWorld {
  public static void main(String[] args) {
    System.out.println("+---------------------------+");
    System.out.println("|       Hallo World!        |");
    System.out.println("+---------------------------+");
  }
}' > "$1/localRepo1/HelloWorld.java"
git -C "$1/localRepo1" add HelloWorld.java
mkdir "$1/localRepo1/doc"
echo 'HelloWorld gibt "Hello World" auf dem Standard-Output-Stream aus.' > "$1/localRepo1/doc/README.txt"
git -C "$1/localRepo1" add doc/README.txt
git -C "$1/localRepo1" commit -m "Initiale Version von HelloWorld.java und doc/README.txt"

# 2. Commit auf req01
git -C "$1/localRepo1" checkout -b req01
sed -i '3i \ \ \ \ System.out.println("Herzlich Willkommen\\n");' "$1/localRepo1/HelloWorld.java"
echo "" >> "$1/localRepo1/doc/README.txt" #ToDo: sed mag keine \n am Anfang des Ersetzungstexts
sed -i '2a Change Log:\n\n* Kasten um Hello World' "$1/localRepo1/doc/README.txt"
git -C "$1/localRepo1" commit -am "Willkommensgruß eingefügt"

# 3. Commit auf master
git -C "$1/localRepo1" checkout master
sed -i 's/Hallo World!/Hello World!/g' "$1/localRepo1/HelloWorld.java"
echo "" >> "$1/localRepo1/doc/README.txt" #ToDo: sed mag keine \n am Anfang des Ersetzungstexts
sed -i '2a Bugfix:\n\n* Typo: "Hello World" statt "Hallo World"' "$1/localRepo1/doc/README.txt"
git -C "$1/localRepo1" commit -am "Typo beseitigt"

# 4. Merge durchführen
git -C "$1/localRepo1" merge req01
echo 'HelloWorld gibt "Hello World" auf dem Standard-Output-Stream aus.

Change Log:
* Kasten um Hello World
* Typo: "Hello World" statt "Hallo World" 
' > "$1/localRepo1/doc/README.txt"
git -C "$1/localRepo1" commit -am "Merge commit"

# 5. Pushen
git -C "$1/localRepo1" push --all

# 5. Pullen 
git -C "$1/localRepo2" pull --all