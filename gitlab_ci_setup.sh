#!/bin/bash

APPNAME="toolboxcicd"

source <(curl -s https://gitlab.com/rwoerzbe/public/-/raw/main/generic_setup.sh)

if [[ -z "${POSTGRES_PASSWORD}" ]]; then
   POSTGRES_PASSWORD=$(tr -dc A-Za-z0-9 </dev/urandom | head -c 30 ; echo '');
fi

if [ "${STAGE}" -ge 1 ]
then
gcloud projects create ${APPNAME}-${CAMPUS_ID}-${PROJECT_ID_SUFFIX}
gcloud config set project ${APPNAME}-${CAMPUS_ID}-${PROJECT_ID_SUFFIX}
gcloud beta billing projects link ${DEVSHELL_PROJECT_ID} --billing-account=${BILLING}
gcloud services enable compute.googleapis.com
gcloud config set compute/region ${REGION}
gcloud config set compute/zone ${ZONE}
gcloud services enable secretmanager.googleapis.com sqladmin.googleapis.com run.googleapis.com containerregistry.googleapis.com cloudbuild.googleapis.com servicenetworking.googleapis.com cloudresourcemanager.googleapis.com
PROJECT_NUMBER=$(gcloud projects list --filter=$DEVSHELL_PROJECT_ID --format='value(PROJECT_NUMBER)')
gcloud projects add-iam-policy-binding ${DEVSHELL_PROJECT_ID} --member="serviceAccount:$PROJECT_NUMBER@cloudbuild.gserviceaccount.com" --role="roles/run.admin"
gcloud projects add-iam-policy-binding ${DEVSHELL_PROJECT_ID} --member="serviceAccount:$PROJECT_NUMBER@cloudbuild.gserviceaccount.com" --role="roles/iam.serviceAccountUser"
gcloud iam service-accounts keys create ~/$PROJECT_NUMBER-compute@developer.gserviceaccount.com-credentials.json --iam-account=$PROJECT_NUMBER-compute@developer.gserviceaccount.com
gcloud projects add-iam-policy-binding ${DEVSHELL_PROJECT_ID} --member="user:rene.woerzberger@gmail.com" --role="roles/run.developer"
gcloud projects add-iam-policy-binding ${DEVSHELL_PROJECT_ID} --member="serviceAccount:$PROJECT_NUMBER-compute@developer.gserviceaccount.com" --role="roles/run.admin"
 
gcloud sql instances create sqlinstance-01 \
--database-version=POSTGRES_14 \
--tier=db-f1-micro `# costs about 10 EUR per month` \
--region=${REGION} `# we must set this deliberately` \
--authorized-networks=0.0.0.0/0 `# we need that for connecting PostgreSQL Explorer and the like` \
--database-flags=max_connections=132 `# = 8 connections per container (as per spring.datasource.hikari.maximum-pool-size = 8) * 4 containers (max) * 2 stages * 2 deployments + 3 superuser reserved connections + 1`

gcloud sql users set-password postgres --instance=sqlinstance-01 --password=${POSTGRES_PASSWORD}
gcloud sql databases create staging --instance=sqlinstance-01
gcloud sql databases create production --instance=sqlinstance-01 
gcloud projects add-iam-policy-binding ${DEVSHELL_PROJECT_ID} --member="serviceAccount:$PROJECT_NUMBER-compute@developer.gserviceaccount.com"  --role="roles/cloudsql.client"

gcloud secrets create postgres_password
echo ${POSTGRES_PASSWORD} | gcloud secrets versions add postgres_password --data-file=-
gcloud projects add-iam-policy-binding ${DEVSHELL_PROJECT_ID} --member="serviceAccount:$PROJECT_NUMBER-compute@developer.gserviceaccount.com"  --role="roles/secretmanager.secretAccessor"

echo "The .gitlab-ci.yml contains variables, which must be set in your GitLab project via 
Settings / CICD / Variables / Add variable. 
(Uncheck all checkboxes (protect, mask, expand) for all variables)
- For GCP_CLOUD_SQL_INSTANCE_CONNECTION_NAME: ${DEVSHELL_PROJECT_ID}:${REGION}:sqlinstance-01
- For GCP_PROJECT_ID: ${DEVSHELL_PROJECT_ID}
- For GCP_CLOUD_BUILD_SERVICE_KEY use the whole following JSON including the outmost curly brackets:"
cat ~/$PROJECT_NUMBER-compute@developer.gserviceaccount.com-credentials.json

echo "If you want to access the Cloud SQL Postgres database directly with PgAdmin and the like, use
Connection Name: Arbitrary. I would suggest ${DEVSHELL_PROJECT_ID}:${REGION}:sqlinstance-01 which is the abovementioned GCP_CLOUD_SQL_INSTANCE_CONNECTION_NAME 
Host: $(gcloud sql instances list --filter=name:sqlinstance-01 --format='value(PRIMARY_ADDRESS)')
User: postgres
Port: 5432
SSL / TLS / Secure Connection: true
Database: postgres
Password: Look that up in https://console.cloud.google.com/security/secret-manager/secret/postgres_password/versions?project=${DEVSHELL_PROJECT_ID} under Actions / View Secret Value"
fi