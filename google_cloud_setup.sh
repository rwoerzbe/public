#!/bin/bash

APPNAME="toolbox"

source <(curl -s https://gitlab.com/rwoerzbe/public/-/raw/main/generic_setup.sh)

if [ "${APPNAME}" == "parcer" ]
then
  JDKVERSION=11
  HEALTH_CHECK_URL="/customers"
elif [ "${APPNAME}" == "pollapp" ]
then
  JDKVERSION=17
  HEALTH_CHECK_URL="/polls"
elif [ "${APPNAME}" == "toolbox" ]
then
  JDKVERSION=21
  HEALTH_CHECK_URL="/"
fi

if [ "${STAGE}" -ge 1 ]
then
gcloud projects create ${APPNAME}-${CAMPUS_ID}-${PROJECT_ID_SUFFIX}
gcloud config set project ${APPNAME}-${CAMPUS_ID}-${PROJECT_ID_SUFFIX}
gcloud beta billing projects link ${DEVSHELL_PROJECT_ID} --billing-account=${BILLING}
gcloud services enable compute.googleapis.com
sleep 30
gcloud config set compute/region ${REGION}
gcloud config set compute/zone ${ZONE}
fi

if [ "${STAGE}" -ge 2 ]
then
gcloud services enable sqladmin.googleapis.com
sleep 30
gcloud sql instances create sqlinstance-01 --cpu=1 --memory=3840MiB --database-version=POSTGRES_13 --region=${REGION}
gcloud sql users set-password postgres --instance=sqlinstance-01 --password=x79bi1vzU77
fi

if [ "${STAGE}" -ge 3 ]
then
curl -o ~/${APPNAME}-with-postgres.jar -L https://gitlab.com/rwoerzbe/public/-/raw/main/${APPNAME}-with-postgres.jar
gsutil mb gs://${DEVSHELL_PROJECT_ID}
gsutil cp ~/${APPNAME}-with-postgres.jar gs://${DEVSHELL_PROJECT_ID}
fi

if [ "${STAGE}" -ge 4 ]
then
PROJECT_NUMBER=$(gcloud projects list --filter=$DEVSHELL_PROJECT_ID --format='value(PROJECT_NUMBER)')
SERVICE_ACCOUNT=$PROJECT_NUMBER-compute@developer.gserviceaccount.com
gcloud iam service-accounts keys create ~/credentials.json --iam-account=$SERVICE_ACCOUNT
gsutil cp ~/credentials.json gs://${DEVSHELL_PROJECT_ID}
fi

if [ "${STAGE}" -ge 5 ]
then
gcloud compute instances create vminstance-01 --zone=${ZONE} \
--image-project ubuntu-os-cloud \
--image-family ubuntu-2204-lts \
--metadata=startup-script="#! /bin/bash
sudo apt update -y
sudo apt install openjdk-${JDKVERSION}-jdk-headless -y
curl -sSO https://dl.google.com/cloudagents/add-google-cloud-ops-agent-repo.sh
sudo bash add-google-cloud-ops-agent-repo.sh --also-install
export PROJECT_ID="'$(curl "http://metadata.google.internal/computeMetadata/v1/project/project-id" -H "Metadata-Flavor: Google")'"
export GOOGLE_APPLICATION_CREDENTIALS=/root/credentials.json
export CLOUD_SQL_INSTANCE_CONNECTION_NAME="'$PROJECT_ID'":${REGION}:sqlinstance-01
gsutil cp gs://"'$PROJECT_ID'"/credentials.json "'$GOOGLE_APPLICATION_CREDENTIALS'"
gsutil cp gs://"'$PROJECT_ID'"/${APPNAME}-with-postgres.jar /root/${APPNAME}-with-postgres.jar
java -Dspring.cloud.gcp.sql.instance-connection-name="'$CLOUD_SQL_INSTANCE_CONNECTION_NAME'" -Dspring.cloud.gcp.sql.database-name=postgres -Dspring.datasource.password=x79bi1vzU77 -jar /root/${APPNAME}-with-postgres.jar"
gcloud compute firewall-rules create ${APPNAME}http --allow tcp:8080
fi

if [ "${STAGE}" -ge 6 ]
then
gcloud compute instance-templates create instancetemplate-01 \
--image-project ubuntu-os-cloud \
--image-family ubuntu-2204-lts \
--tags=http-server,https-server \
--metadata=startup-script="#! /bin/bash
sudo apt update -y
sudo apt install openjdk-${JDKVERSION}-jdk-headless -y
curl -sSO https://dl.google.com/cloudagents/add-google-cloud-ops-agent-repo.sh
sudo bash add-google-cloud-ops-agent-repo.sh --also-install
export PROJECT_ID="'$(curl "http://metadata.google.internal/computeMetadata/v1/project/project-id" -H "Metadata-Flavor: Google")'"
export GOOGLE_APPLICATION_CREDENTIALS=/root/credentials.json
export CLOUD_SQL_INSTANCE_CONNECTION_NAME="'$PROJECT_ID'":${REGION}:sqlinstance-01
gsutil cp gs://"'$PROJECT_ID'"/credentials.json "'$GOOGLE_APPLICATION_CREDENTIALS'"
gsutil cp gs://"'$PROJECT_ID'"/${APPNAME}-with-postgres.jar /root/${APPNAME}-with-postgres.jar
java -Dspring.cloud.gcp.sql.instance-connection-name="'$CLOUD_SQL_INSTANCE_CONNECTION_NAME'" -Dspring.cloud.gcp.sql.database-name=postgres -Dspring.datasource.password=x79bi1vzU77 -jar /root/${APPNAME}-with-postgres.jar"
gcloud compute instance-groups managed create instancegroup-01 --template instancetemplate-01 --size=2
fi

if [ "${STAGE}" -ge 7 ]
then
gcloud sql instances patch sqlinstance-01 --backup-start-time=01:00
gcloud sql instances patch sqlinstance-01 --availability-type REGIONAL
gcloud sql instances create sqlinstance-03 --master-instance-name=sqlinstance-01 --region=${REGION}
fi

if [ "${STAGE}" -ge 8 ]
then
gcloud compute instance-groups managed set-named-ports instancegroup-01 --named-ports=${APPNAME}-http:8080
gcloud compute health-checks create http healthcheck-01 --port-name=${APPNAME}-http --request-path=${HEALTH_CHECK_URL}
gcloud compute backend-services create backendservice-01 --health-checks=healthcheck-01 --port-name=${APPNAME}-http --global
gcloud compute backend-services add-backend backendservice-01 --instance-group=instancegroup-01 --instance-group-zone=${ZONE} --global
gcloud compute url-maps create urlmap-01 --default-service=backendservice-01
gcloud compute target-http-proxies create targethttpproxy-01 --url-map=urlmap-01
gcloud compute addresses create address-01 --ip-version=IPV4 --global
gcloud compute forwarding-rules create forwardingrule-01 --address=address-01 --target-http-proxy=targethttpproxy-01 --ports=80 --global
fi

if [ "${STAGE}" -ge 9 ]
then
gcloud services enable domains.googleapis.com
gcloud services enable dns.googleapis.com
sleep 30
gcloud dns managed-zones create managedzone-01 --dns-name=${DEVSHELL_PROJECT_ID}.com --description=
if [[ ! -f contacts.yaml ]]
then
cat > contacts.yaml <<EOF
allContacts:
  email: 'rene.woerzberger@th-koeln.de'
  phoneNumber: '+49.22182754324 '
  postalAddress:
    regionCode: 'DE'
    postalCode: '50679'
    locality: 'Koeln'
    addressLines: ['Betzdorfer Str. 2']
    recipients: ['Rene Woerzberger']
EOF
fi
echo "y" | gcloud domains registrations register ${DEVSHELL_PROJECT_ID}.com --cloud-dns-zone=managedzone-01 --contact-data-from-file=contacts.yaml
while [ $(gcloud domains registrations describe ${DEVSHELL_PROJECT_ID}.com --format='value(state)') != "ACTIVE" ]
do
    echo Registration of domain ${DEVSHELL_PROJECT_ID}.com still in state $(gcloud domains registrations describe ${DEVSHELL_PROJECT_ID}.com --format='value(state)'). Stay patient!
    sleep 10
done
rm contacts.yaml
FW_EXTERNAL_IP=$(gcloud compute addresses describe address-01 --global --format='value(address)')
gcloud dns record-sets transaction start --zone=managedzone-01
gcloud dns record-sets transaction add $FW_EXTERNAL_IP --name=${DEVSHELL_PROJECT_ID}.com. --ttl=7200 --type=A --zone=managedzone-01
gcloud dns record-sets transaction execute --zone=managedzone-01
fi

if [ "${STAGE}" -ge 10 ]
then
gcloud compute ssl-certificates create sslcertificate-01 --domains=${DEVSHELL_PROJECT_ID}.com
sleep 30
gcloud compute target-https-proxies create targethttpsproxy-01 --url-map=urlmap-01 --ssl-certificates=sslcertificate-01
gcloud compute forwarding-rules create forwardingrule-02 --address=address-01 --target-https-proxy=targethttpsproxy-01 --ports=443 --global
fi

if [ "${STAGE}" -ge 11 ]
then
gcloud services enable logging.googleapis.com
fi