#!/bin/bash -vx
if [ "$#" -lt 2 ]
then
echo "Automates the assignment 'Web Application Layers Protocols' 
(cf. https://gitlab.nt.fh-koeln.de/gitlab/common/exercises/-/blob/master/exercises/web_application_layer_protocols_en.asciidoc).
All resources are assigned to a new project "'"${APPNAME}-$CAMPUS_ID-$RANDOM"'", where $CAMPUS_ID stands for your 
campus ID and $RANDOM for a random integer between 0 and 32767.

usage:" $(basename "$0")" CAMPUS_ID ZONE [STAGE] 

CAMPUS_ID stands for your campus ID (like rwoerzbe)
ZONE stands for a valid zone like europe-west4-a 
     (cf. https://cloud.google.com/compute/docs/regions-zones?hl=en#available )
STAGE is optional. If specified as N, the script only creates the cluster up 
      to (and including) the instructions in Section N of the assignment.
      If omitted, all steps of the exercise will be executed.
APPNAME is optional. If specified, STAGE must also be specified (since both are
        positional parameters). The APPNAME refers to the actual name of the prepared
        Spring-Boot based app that is supposed to be deployed. Valid values so far are:
        parcer, pollapp.
        APPNAME defaults to 'parcer' if not specified."
exit 1
fi

export STAGE=${3:-9999}

if [ "${STAGE}" -ge 1 ]
then
export CAMPUS_ID=$1
export PROJECT_ID_SUFFIX=$RANDOM
export ZONE=$2
export REGION=${ZONE::-2}
export DEVSHELL_PROJECT_ID=jhipster-${CAMPUS_ID}-${PROJECT_ID_SUFFIX}
gcloud projects create jhipster-${CAMPUS_ID}-${PROJECT_ID_SUFFIX}
gcloud config set project jhipster-${CAMPUS_ID}-${PROJECT_ID_SUFFIX}
gcloud beta billing projects link ${DEVSHELL_PROJECT_ID} --billing-account=$(gcloud beta billing accounts list --filter='open:true' --format='value(ACCOUNT_ID)' | head -n 1)
gcloud services enable compute.googleapis.com
gcloud config set compute/region ${REGION}
gcloud config set compute/zone ${ZONE}
gcloud compute instances create vminstance-01 --zone=${ZONE} \
--image-project ubuntu-os-cloud \
--image-family ubuntu-2004-lts \
--metadata=startup-script="#! /bin/bash
sudo apt update -y
sudo apt install openjdk-11-jdk-headless -y
mkdir -p ~/jhipster-reworate/target
curl -sL -o ~/jhipster-reworate/target/reworate-0.0.1-SNAPSHOT.jar https://gitlab.com/rwoerzbe/public/-/raw/main/reworate-0.0.1-SNAPSHOT.jar
java -jar ~/jhipster-reworate/target/reworate-0.0.1-SNAPSHOT.jar"
gcloud compute firewall-rules create ${APPNAME}http --allow tcp:8080
fi


